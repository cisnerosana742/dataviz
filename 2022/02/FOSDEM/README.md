# README

The aim here was to show the massive scale of FOSDEM 2022's Matrix presence.

A small raster version of the final result is below, along with some zoomed
sections. Every rooms' title is present and they are arranged under their spaces
in hierarchy. The original is a large 8MB SVG which you can find
[here](FOSDEM.svg).

It was posted on social media:

* [Pixelfed](https://pixelfed.social/p/pixian/399266820995675277)
* [Twitter](https://twitter.com/MrCrufts/status/1493654367437828099)

Thanks to the following for making this possible:

* [Draw Freely | Inkscape](https://inkscape.org/)
* [Asymptote: The Vector Graphics Language](https://asymptote.sourceforge.io/)
* [Matrix.org](https://matrix.org/)
* [FOSDEM 2022](https://fosdem.org/2022/)

![A map of the #fosdem 2022 #matrix space and rooms](FOSDEM.png)

![Zoom in on the miscellaneous rooms](Screenshot from 2022-02-15 17-37-50.png)
![Zoom in on the end of the devrooms and the first few stand rooms](Screenshot from 2022-02-15 17-41-00.png)

## Future Improvement

* It's a shame the SVG has paths for the text rather than 'real' text, so you
  can't easily search it.
* We couldn't cope with the scale and had to compose the final SVG from 4
  separate ones. Be good to drill into why this was.
* It would be interesting to make it update live with maybe colours and sizes of
  the room to represent number of members and/or level of activity. Then you
  could see where the action was during the event. Click on the rooms to join
  them even.

## Process

The process is super scrappy. In the end I simply copy/pasted the FOSDEM space
listing straight out of my Element client ([FOSDEM.txt](FOSDEM.txt)) and did a
bit of text processing on it. I looked at using [poljar/matrix-nio: A Python
Matrix client library, designed according to sans I/O
(http://sans-io.readthedocs.io/)
principles](https://github.com/poljar/matrix-nio) but at a quick glance couldn't
see support for Matrix spaces, only rooms. So in the end for fast results I went
the text/copy/paste route.

### Prerequisites

Install (my own project) [figular](https://pypi.org/project/figular/) - `pip
install figular`. Note it has its own dependencies.

### Usage

There's an assumption Figular is installed in the user's site-packages below in
order to locate it.

```bash
# Process FOSDEM.txt into a form Figular can process
# Use awk to split it into multiple files as size
# is an issue
./FOSDEM.py | awk 'BEGIN { i = 0; } /^# --/ { i = i+1; } { print $0 > "out_"i }'
# Run Asymptote against a tweaked version of Figular's orgchart
# across each output file from above
find . -name "out*" | xargs -n1 bash -c \
  'cat "$0" | asy -autoimport $(python -m site --user-site)/figular/org/orgchart.asy draw.asy -f svg -o "$0".svg'
```

This gets you three big SVGs which I then manually worked into one giant 8MB
SVG with Inkscape, then coloured and titled. Kudos to Inkscape for handling such
a big SVG with over 30,000 objects.
