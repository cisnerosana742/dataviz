# dataviz

Collection of my data visualisations:

* FOSDEM - [README](2022/02/FOSDEM/README.md)
* OSI - [README](2022/03/OSI/README.md)

## Licensing

* All source code is licensed under AGPL-3.0-or-later.
* Anything else that is not executable, including the text when extracted from
  code, is licensed under CC-BY-SA-4.0.
